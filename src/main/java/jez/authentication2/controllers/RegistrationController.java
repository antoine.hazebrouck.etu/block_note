package jez.authentication2.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jez.authentication2.entities.User;
import jez.authentication2.services.UserService;

@Controller
public class RegistrationController {
	@Autowired
	UserService userService;

	@GetMapping("/register")
	public String displayRegistrationPage(Model model) {
		model.addAttribute("user", new User());
		return "registration_page";
	}

	@PostMapping("/register")
	public String registerUser(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes)
			throws SQLException {
		user.setEnabled(true);
		user.setAuthority("ROLE_USER");
		user.setNotes(new ArrayList<>());
		
		// Note newNote = new Note("new note", user);
		// user.getNotes().add(newNote);
		userService.saveUser(user);
		System.out.println("id:" + userService.findUserByUsername(user.getUsername()).getId());
		return "redirect:/";
	}
}

package jez.authentication2.controllers;

import java.util.Comparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import jez.authentication2.entities.Note;
import jez.authentication2.entities.User;
import jez.authentication2.services.NotesService;
import jez.authentication2.services.UserService;

@Controller
public class MainController {
	@Autowired
	UserService userService;
	@Autowired
	NotesService notesService;

	@GetMapping("/")
	public String displayHome() {
		return "home.html";
	}

	@GetMapping(value = "/notes")
	public String displayNotes() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByUsername(authentication.getName());


		// si l'utilisateur a 0 notes
		if (user.getNotes().isEmpty()) {
			createNewNote(user);
		}
		// user = userService.findUserByUsername(user.getUsername());

		return "redirect:/notes/%s".formatted(user.getNotes().get(0).getId());
	}

	@GetMapping(value = "/notes/{note_id}")
	public String displayNotes(@PathVariable Integer note_id, Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByUsername(authentication.getName());

		// si l'utilisateur a 0 notes
		if (user.getNotes().isEmpty()) {
			createNewNote(user);
		}

		// si la note $note_id n'appartient pas a l'utilisateur
		Boolean note_idNotInUserNotes = false;
		for (Note note : user.getNotes()) {
			if (note.getId() == note_id) {
				note_idNotInUserNotes = true;
			}
		}
		if (note_idNotInUserNotes == false) {
			return "redirect:/";
		}
		// if (user.getNotes().stream().anyMatch(new Predicate<Note>() {
		// @Override
		// public boolean test(Note note) {
		// return note.getId() == note_id;
		// }
		// })) {
		// return "redirect:/";
		// }



		user.getNotes().sort(new Comparator<Note>() {
			@Override
			public int compare(Note o1, Note o2) {
				return o1.getId() - o2.getId();
			}
		});

		model.addAttribute("note_id", note_id);
		model.addAttribute("notes", notesService.findById(note_id));
		model.addAttribute("allNotes", user.getNotes());

		return "main_page.html";
	}

	@PostMapping(value = "/register_notes/{note_id}")
	public String registerNotes(@PathVariable Integer note_id,
			@RequestParam(name = "notes") String notes) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByUsername(authentication.getName());

		Note current = notesService.findById(note_id);
		current.setNote(notes);
		notesService.saveNote(current);


		// select * from users join notes on users.id=notes.user_id;
		// select * from notes;
		return "redirect:/notes/%s".formatted(note_id);
	}

	@PostMapping(value = "/create_notes")
	public String createNotes() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByUsername(authentication.getName());

		Integer newNoteId = createNewNote(user);

		return "redirect:/notes/%s".formatted(newNoteId);
	}

	private Integer createNewNote(User user) {
		Note newNote = new Note("new note", user);
		notesService.saveNote(newNote);
		user.getNotes().add(newNote);
		System.out.println("id => " + newNote.getId());

		return newNote.getId();
	}

	@GetMapping(value = "/user")
	public String user() {
		return ("user");
	}

	@GetMapping(value = "/admin")
	public String admin() {
		return ("<h1> Welcome Admin</h1>");
	}
}

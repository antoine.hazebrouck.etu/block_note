package jez.authentication2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import jez.authentication2.entities.Note;

public interface NotesRepository extends JpaRepository<Note, Integer> {

}

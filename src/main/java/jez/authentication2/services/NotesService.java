package jez.authentication2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jez.authentication2.entities.Note;
import jez.authentication2.repositories.NotesRepository;

@Service
public class NotesService {
	@Autowired
	NotesRepository notesRepository;

	public void saveNote(Note note) {
		notesRepository.save(note);
	}

	public Note findById(Integer id)
	{
		return notesRepository.findById(id).orElse(null);
	}
}

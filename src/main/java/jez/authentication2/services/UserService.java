package jez.authentication2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jez.authentication2.entities.User;
import jez.authentication2.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public void saveUser(User user) {
		userRepository.save(user);
	}

	public User findUserByUsername(String username)
	{
		return userRepository.findByUsername(username).get(0);
	}
}

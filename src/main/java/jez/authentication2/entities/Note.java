package jez.authentication2.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "notes")
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String title;
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	private String note;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Note() {
	}

	public Note(Integer id, String note, User user) {
		this.id = id;
		this.note = note;
		this.user = user;
	}

	public Note(String note, User user) {
		this.note = note;
		this.user = user;
	}

	public Note(Integer id, String note) {
		this.id = id;
		this.note = note;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", note=" + note + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	
}

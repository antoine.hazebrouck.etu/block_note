drop table if exists users cascade;
create table users(
	id serial primary key,
	username varchar(50) not null unique,
	password varchar(50) not null,
	enabled boolean not null,
	authority varchar(10) not null
);

-- drop table if exists authorities cascade;
-- create table authorities (
-- 	username varchar(50) not null,
-- 	authority varchar(50) not null,
-- 	foreign key(username) references users(username)
-- );

drop table if exists notes cascade;
create table notes (
	id serial primary key,
	title text,
	note text,
	user_id int not null,
	foreign key(user_id) references users(id)
);

	-- constraint fk_authorities_users 
-- create unique index ix_auth_username on authorities (username,authority);
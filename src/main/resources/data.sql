insert into users (username, password, enabled, authority)
values ('user', 'password', true, 'ROLE_USER');

insert into notes (user_id, title, note)
values (1, 'voala', 'some  ...1');
insert into notes (user_id, title, note)
values (1, 'course', 'some notes ...2');

insert into users (username, password, enabled, authority)
values ('admin', 'password', true, 'ROLE_ADMIN');

-- insert into authorities (username, authority)
-- values ('user', 'ROLE_USER');

-- insert into authorities (username, authority)
-- values ('admin', 'ROLE_ADMIN');
